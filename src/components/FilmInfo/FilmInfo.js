import React, {useEffect, useState} from 'react';
import {Card, CardBody, CardImg, CardTitle, Col, Row} from "reactstrap";
import axiosFilms from "../../axiosFilms";

const FilmInfo = (props) => {
    const initialImage = {};
    const initialFilm = {};
    const [film, setFilm] = useState(initialFilm);
    const [image, setImage] = useState(initialImage);


    const fetchFilm = async () => {
        const response = await axiosFilms("/shows/" + props.match.params.id);
        setFilm(response.data);
        setImage(response.data.image);
    };

    useEffect(() => {
        fetchFilm();
    }, [props.match.params.id]);

    function createMarkup() {
        return {__html: film.summary};
    }
    return (
        <Card style={{zIndex:"-1"}}>
            <Row>
                <Col md={4} lg={3}>
                    <CardImg style={{width: "200px"}} top src={image.original} alt={film.name}/>
                </Col>
                <Col md={8} lg={9}>
                    <CardBody>
                        <CardTitle><h3>{film.name}</h3></CardTitle>
                        <div dangerouslySetInnerHTML={createMarkup()}/>
                    </CardBody>
                </Col>
            </Row>
        </Card>
    );
};

export default FilmInfo;