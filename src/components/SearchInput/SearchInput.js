import React, {useState} from 'react';
import {Col, Form, FormGroup, Input, Label} from "reactstrap";
import Autocomplete from 'react-autocomplete';
import axiosFilms from "../../axiosFilms";
import {NavLink} from "react-router-dom";

const SearchInput = (props) => {
    const initialChangeValue = "";
    const initialShows = [];
    const [changeValue, setChangeValue] = useState(initialChangeValue);
    const [shows, setShows] = useState(initialShows);


    const valueChanged = async event => {
        event.persist();
        setChangeValue(() => {
            return event.target.value
        });
        const response = await axiosFilms("/search/shows?q=" + event.target.value);
        setShows(response.data);
    };

    return (
        <Form style={{marginTop:"15px", textAlign:"center"}}>
            <FormGroup row>
                <Label for="exampleShowName" sm={4}>Search for TV Show</Label>
                <Col sm={8}>
                    <Autocomplete style={{width:"100%"}}
                        getItemValue={(item) => item.show.name}
                        items={shows}
                        renderItem={(item, isHighlighted) =>
                            <Input tag={NavLink} to={'/shows/' + item.show.id } exact key={item.show.id} style={{background: isHighlighted ? 'lightgray' : 'white'}}>
                                {item.show.name}
                            </Input>
                        }
                        value={changeValue}
                        onChange={e => valueChanged(e)}
                        onSelect={(val) => setChangeValue(() => {
                            return val;
                        })}

                    />
                </Col>
            </FormGroup>
        </Form>
    );
};

export default SearchInput;