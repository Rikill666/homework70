import React from 'react';
import {Container} from "reactstrap";
import {Route, Switch} from "react-router-dom";
import SearchPage from "./components/SearchPage/SearchPage";
import FilmInfo from "./components/FilmInfo/FilmInfo";
import SearchInput from "./components/SearchInput/SearchInput";

const App = () => {
    return (
        <Container>
            <SearchInput/>
            <Switch>
                <Route path="/" exact component={SearchPage}/>
                <Route path="/shows/:id" exact component={FilmInfo}/>
                <Route render={() => <h1>Not Found</h1>}/>
            </Switch>
        </Container>
    );
};

export default App;
